from django.shortcuts import render, redirect
from Eshop.models import Product, Categories, FILTER_PRICE, Color, Brand, Contact_us
from django.conf import settings
from django.core.mail import send_mail
from django.contrib.auth.models import User
from django.contrib.auth import authenticate,login,logout


import razorpay

client = razorpay.Client(auth=(settings.RAZORPAY_KEY_ID, settings.RAZORPAY_KEY_SECRET))


def BASE(request):
    return render(request, 'main/base.html')


def HOME(request):
    product = Product.objects.filter(status="Publish")
    context = {
        'product': product,
    }
    return render(request, 'main/index.html', context)


def PRODUCTS(request):
    categories = Categories.objects.all()
    filter_price = FILTER_PRICE.objects.all()
    color = Color.objects.all()
    brand = Brand.objects.all()

    CATID = request.GET.get('categories')
    FILTER_PRICE_ID = request.GET.get('filter_price')
    COLOR_ID = request.GET.get('color')
    BRAND_ID = request.GET.get('brand')

    ATOZID = request.GET.get('ATOZ')
    ZTOAID = request.GET.get('ZTOA')
    LOWTOHIGHID = request.GET.get('LOWTOHIGH')
    HIGHTOLOWID = request.GET.get('HIGHTOLOW')
    NEW_PRODUCTID = request.GET.get('NEWPRODUCT')
    OLD_PRODUCTID = request.GET.get('OLDPRODUCT')

    if CATID:
        product = Product.objects.filter(Categories=CATID, status="Publish")

    elif FILTER_PRICE_ID:
        product = Product.objects.filter(filter_price=FILTER_PRICE_ID, status="Publish")

    elif COLOR_ID:
        product = Product.objects.filter(color=COLOR_ID, status="Publish")

    elif BRAND_ID:
        product = Product.objects.filter(brand=BRAND_ID)

    elif ATOZID:
        product = Product.objects.filter(status="Publish").order_by('name')

    elif ZTOAID:
        product = Product.objects.filter(status='Publish').order_by('-name')

    elif LOWTOHIGHID:
        product = Product.objects.filter(status='Publish').order_by('price')

    elif HIGHTOLOWID:
        product = Product.objects.filter(status='Publish').order_by('-price')

    elif NEW_PRODUCTID:
        product = Product.objects.filter(status='Publish', condition='New')

    elif OLD_PRODUCTID:
        product = Product.objects.filter(status='Publish', condition='Old')

    else:
        product = Product.objects.filter(status="Publish")

    context = {
        'product': product,
        'categories': categories,
        'f_price': filter_price,
        'color': color,
        'brand': brand
    }

    return render(request, 'main/product.html', context)


def SEARCH(request):
    query = request.GET.get('query')
    product = Product.objects.filter(name__icontains=query)

    context = {
        'product': product
    }

    return render(request, 'main/search.html', context)


def PRODUCT_DETAIL_PAGE(request, id):
    prod = Product.objects.filter(id=id).first()

    context = {
        'prod': prod,
    }
    return render(request, 'main/product_single.html', context)


def Contact_Page(request):
    if request.method == 'POST':
        name = request.POST.get('name')
        email = request.POST.get('email')
        subject = request.POST.get('subject')
        message = request.POST.get('message')

        contact = Contact_us(

            name=name,
            email=email,
            subject=subject,
            message=message

        )

        subject = subject
        message = message
        email_from = settings.EMAIL_HOST_USER
        try:
            send_mail(subject, message, email_from, ['rushabhs@webelight.co.in'])
            contact.save()
            return redirect('home')
        except:
            return redirect('contact')
    return render(request, 'main/contact.html')



def HandleRegister(request):
    if request.method == "POST":
        username = request.POST.get('username')
        first_name = request.POST.get('first_name')
        last_name = request.POST.get('last_name')
        email = request.POST.get('email')
        pass1 = request.POST.get('pass1')
        pass2 = request.POST.get('pass2')

        customer = User.objects.create_user(username,email,pass1)
        customer.first_name = first_name
        customer.last_name = last_name
        customer.save()
        return redirect('home')
    return render(request, 'registration/auth.html')


def HandleLogin(request):
    if request.method == "POST":
        username = request.POST.get('username')
        password= request.POST.get('password')

        user = authenticate(username = username,password = password)
        if user is not None:
            login(request,user)
            return redirect('home')
        else:
            return redirect('login')
    return render(request, 'registration/auth.html')


def HandleLogout(request):
    logout(request)

    return redirect('home')




